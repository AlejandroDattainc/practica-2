import { Injectable } from '@angular/core';

@Injectable()
export class DataService{

    ventas: any[];
    constructor(){
        this.ventas = [
            {
                tipo: 'Estacionario',
                monto: 405903,
                aportacion: 30,
                estado: 'Activo'
            },
            {
                tipo: 'Portatil',
                monto: 2094003,
                aportacion: 35,
                estado: 'Activo'
            },            {
                tipo: 'Carburacion',
                monto: 4405903,
                aportacion: 15,
                estado: 'Activo'
            },
            {
                tipo: 'Venta directa',
                monto: 5605903,
                aportacion: 20,
                estado: 'Activo'
            }
        ]
    }

    // metodo para traer las ventas
    getVentas(){
        return this.ventas;
    }
}


// {
//     tipo: '',
//     monto: null,
//     aportacion: null,
//     estado: ''
// }