import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-graficos',
  templateUrl: './graficos.component.html',
  styleUrls: ['./graficos.component.scss']
})
export class GraficosComponent implements OnInit {
  titulo_grafico: string = 'Titulo de grafico';
  constructor(private dataservice: DataService) { }

  ngOnInit(): void {
    console.log(this.dataservice.getVentas());
  }

  date: Date = new Date();

  trafficChartData = [
    {
      data: [30, 15, 35, 20],
    }
  ];

  trafficChartLabels = ["Estacionario", "Portatil", "Carburacion", "Venta directa"];

  trafficChartOptions = {
    responsive: true,
    animation: {
      animateScale: true,
      animateRotate: true
    },
    legend: false,
  };

  trafficChartColors = [
    {
      backgroundColor: [
        'rgba(177, 148, 250, 1)',
        'rgba(254, 112, 150, 1)',
        'rgba(132, 217, 210, 1)',
        'rgba(255, 191, 150, 1)',
      ],
      borderColor: [
        'rgba(177, 148, 250, .2)',
        'rgba(254, 112, 150, .2)',
        'rgba(132, 217, 210, .2)',
        'rgba(255, 191, 150, .2)'
      ]
    }
  ];
}
